# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jcarrez <jeremie.carrez@yahoo.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/04/27 14:57:05 by jcarrez           #+#    #+#              #
#    Updated: 2020/05/01 15:24:09 by jcarrez          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

BLACK	=	\033[1;30m
RED		=	\033[1;31m
GREEN	=	\033[1;32m
YELLOW	=	\033[1;33m
BLUE	=	\033[1;34m
PURPLE	=	\033[1;35m
CYAN	=	\033[1;36m
WHITE	=	\033[1;37m
ERASE	=	\033[0m
UP		=	\033[A
CLEAN	=	\033[2K

NAME	=	libft.a

SOURCES	=	\
			\
			\
			ft_ato/ft_ato_i.c \
			ft_ato/ft_ato_ui.c \
			\
			\
			\
			ft_btree/ft_btree_create.c \
			ft_btree/ft_btree_delete.c \
			ft_btree/ft_btree_infix.c \
			ft_btree/ft_btree_insert.c \
			ft_btree/ft_btree_prefix.c \
			ft_btree/ft_btree_rev_infix.c \
			ft_btree/ft_btree_search.c \
			ft_btree/ft_btree_suffix.c \
			\
			\
			\
			ft_gnl/ft_gnl.c \
			\
			\
			\
			ft_i/ft_i_abs.c \
			ft_i/ft_i_cmp.c \
			ft_i/ft_i_factorial.c \
			ft_i/ft_i_max.c \
			ft_i/ft_i_min.c \
			ft_i/ft_i_power.c \
			ft_i/ft_i_sqrt.c \
			\
			\
			\
			ft_is/ft_is.c \
			ft_is/ft_is_alnum.c \
			ft_is/ft_is_alpha.c \
			ft_is/ft_is_ascii.c \
			ft_is/ft_is_d_digit.c \
			ft_is/ft_is_digit.c \
			ft_is/ft_is_h_digit.c \
			ft_is/ft_is_hex.c \
			ft_is/ft_is_lo.c \
			ft_is/ft_is_lo_h.c \
			ft_is/ft_is_print.c \
			ft_is/ft_is_sign.c \
			ft_is/ft_is_up.c \
			ft_is/ft_is_up_h.c \
			ft_is/ft_is_white.c \
			\
			\
			\
			ft_len/ft_len_b.c \
			ft_len/ft_len_h.c \
			ft_len/ft_len_i.c \
			ft_len/ft_len_ms.c \
			ft_len/ft_len_o.c \
			ft_len/ft_len_ss.c \
			ft_len/ft_len_st.c \
			ft_len/ft_len_str.c \
			ft_len/ft_len_ui.c \
			ft_len/ft_len_x.c \
			\
			\
			\
			ft_list/ft_list_add.c \
			ft_list/ft_list_append.c \
			ft_list/ft_list_count.c \
			ft_list/ft_list_del.c \
			ft_list/ft_list_delone.c \
			ft_list/ft_list_iter.c \
			ft_list/ft_list_map.c \
			ft_list/ft_list_new.c \
			ft_list/ft_list_rev.c \
			\
			\
			\
			ft_math/ft_swap_bits.c \
			\
			\
			\
			ft_mem/ft_mem_alloc.c \
			ft_mem/ft_mem_ccpy.c \
			ft_mem/ft_mem_chr.c \
			ft_mem/ft_mem_cmp.c \
			ft_mem/ft_mem_cpy.c \
			ft_mem/ft_mem_del.c \
			ft_mem/ft_mem_expand.c \
			ft_mem/ft_mem_move.c \
			ft_mem/ft_mem_realloc.c \
			ft_mem/ft_mem_set.c \
			\
			\
			\
			ft_put/ft_put.c \
			ft_put/ft_put_char.c \
			ft_put/ft_put_char_fd.c \
			ft_put/ft_put_endl.c \
			ft_put/ft_put_fd.c \
			ft_put/ft_put_ms.c \
			ft_put/ft_put_nchar.c \
			ft_put/ft_put_nchar_fd.c \
			ft_put/ft_put_ss.c \
			ft_put/ft_put_st.c \
			ft_put/ft_put_str.c \
			\
			\
			\
			ft_str/ft_str_cat.c \
			ft_str/ft_str_chr.c \
			ft_str/ft_str_chr_index.c \
			ft_str/ft_str_clr.c \
			ft_str/ft_str_cmp.c \
			ft_str/ft_str_cpy.c \
			ft_str/ft_str_del.c \
			ft_str/ft_str_ds.c \
			ft_str/ft_str_dup.c \
			ft_str/ft_str_equ.c \
			ft_str/ft_str_iter.c \
			ft_str/ft_str_iteri.c \
			ft_str/ft_str_join.c \
			ft_str/ft_str_lc.c \
			ft_str/ft_str_lcat.c \
			ft_str/ft_str_map.c \
			ft_str/ft_str_mapi.c \
			ft_str/ft_str_md.c \
			ft_str/ft_str_merge.c \
			ft_str/ft_str_ncat.c \
			ft_str/ft_str_ncmp.c \
			ft_str/ft_str_ncpy.c \
			ft_str/ft_str_ndup.c \
			ft_str/ft_str_nequ.c \
			ft_str/ft_str_new.c \
			ft_str/ft_str_nstr.c \
			ft_str/ft_str_rchr.c \
			ft_str/ft_str_rev.c \
			ft_str/ft_str_rlen.c \
			ft_str/ft_str_sandr.c \
			ft_str/ft_str_split.c \
			ft_str/ft_str_str.c \
			ft_str/ft_str_sub.c \
			ft_str/ft_str_trim.c \
			ft_str/ft_str_uc.c \
			\
			\
			\
			ft_toa/ft_i_toa.c \
			ft_toa/ft_st_toa.c \
			ft_toa/ft_ui_toa.c \
			\
			\
			\



OBJETS	=	${SOURCES:.c=.o}

INCLUDES=	

CC		=	gcc
FLAG	=	-Wall -Werror -Wextra

all : $(NAME)

optimise : FLAG += -Weverything -g3
optimise : all

debug : FLAG += -Weverything -fsanitize=address
debug : all

clean :
	@rm -f $(OBJETS)

fclean : clean
	@rm -f $(NAME)

re : fclean all

$(NAME) : $(OBJETS)
	@echo "$(CLEAN)$(GREEN) All Objets Done"
	@echo "$(CLEAN)$(YELLOW) Creating $(CYAN)$(NAME)"
	@ar rc $(NAME) $(OBJETS)
	@echo "$(UP)$(CLEAN)$(GREEN) Creating $(CYAN)$(NAME) $(GREEN)Done"
	@echo "$(CLEAN)$(YELLOW) Indexing $(CYAN)$(NAME)"
	@ranlib $(NAME)
	@echo "$(UP)$(UP)$(CLEAN)$(GREEN) Indexing $(CYAN)$(NAME) $(GREEN)Done"

%.o : %.c
	@echo "$(CLEAN)$(YELLOW) Creating Objets"
	@echo "$(CLEAN)$(YELLOW)                                    From $(CYAN)$<"
	@echo "$(UP)$(YELLOW) Create $(CYAN)$@"
	@$(CC) $(FLAG) -c -o $@ $<
	@echo "$(UP)$(UP)$(UP)"
