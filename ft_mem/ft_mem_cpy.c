/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mem_cpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 18:28:27 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/20 18:31:39 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_mem.h"

void	*ft_mem_cpy(void *d, const void *s, size_t n)
{
	char	*c1;
	char	*c2;
	size_t	m;
	size_t	c;

	c = 0;
	m = n;
	if (n == 0 || d == s)
		return (d);
	c1 = (char *)d;
	c2 = (char *)s;
	while (--m)
	{
		c1[c] = c2[c];
		c++;
	}
	c1[c] = c2[c];
	return (d);
}
