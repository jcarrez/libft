/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mem_move.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 19:09:17 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 11:00:50 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_mem.h"

void	*ft_mem_move(void *d, const void *s, size_t l)
{
	unsigned long	n;
	unsigned char	*d1;
	unsigned char	*s1;

	n = 0;
	s1 = (unsigned char *)s;
	d1 = (unsigned char *)d;
	if (d > s)
	{
		while (l)
		{
			d1[l - 1] = s1[l - 1];
			l--;
		}
	}
	else
	{
		while (l - n)
		{
			d1[n] = s1[n];
			n++;
		}
	}
	return (d);
}
