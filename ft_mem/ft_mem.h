/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mem.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 19:17:01 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 10:31:53 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MEM_H
# define FT_MEM_H

# include "../libft.h"

void	*ft_mem_alloc(size_t s);
void	*ft_mem_ccpy(void *d, const void *s, int c, size_t n);
void	*ft_mem_chr(const void *s, int c, size_t n);
int		ft_mem_cmp(const void *s1, const void *s2, size_t n);
void	*ft_mem_cpy(void *d, const void *s, size_t n);
void	ft_mem_del(void **ap);
void	*ft_mem_expand(void *s, size_t l);
void	*ft_mem_move(void *d, const void *s, size_t l);
void	*ft_mem_realloc(void *s, size_t l);
void	*ft_mem_set(void *b, int c, size_t l);

#endif
