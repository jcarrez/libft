/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mem_expand.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 19:06:40 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/20 23:37:28 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_mem.h"

void	*ft_mem_expand(void *s, size_t l)
{
	char	*t;
	size_t	n;

	n = ft_len_str(s);
	if (!(t = (char *)malloc(sizeof(char) * (n + 1))))
		return (0);
	ft_mem_cpy(t, s, n);
	ft_mem_realloc(s, l);
	ft_mem_cpy(s, t, n);
	free(t);
	return (s);
}
