/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mem_ccpy.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 18:16:52 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/20 18:19:50 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_mem.h"

void	*ft_mem_ccpy(void *d, const void *s, int c, size_t n)
{
	size_t			m;
	unsigned char	*d2;
	unsigned char	*s2;

	d2 = (unsigned char *)d;
	s2 = (unsigned char *)s;
	m=0;
	while (m < n)
	{
		d2[m] = s2[m];
		if (s2[m] == (unsigned char)c)
			return (d + m + 1);
		m++;
	}
	return (NULL);
}
