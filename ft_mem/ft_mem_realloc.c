/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mem_realloc.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 23:36:20 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/20 23:37:09 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_mem.h"

void	*ft_mem_realloc(void *s, size_t l)
{
	ft_mem_del(&s);
	s = ft_mem_alloc(l);
	return (s);
}
