/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mem_cmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 18:21:41 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/20 18:23:59 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_mem.h"

int		ft_mem_cmp(const void *s1, const void *s2, size_t n)
{
	const unsigned char	*c1;
	const unsigned char	*c2;

	c1 = (const unsigned char *)s1;
	c2 = (const unsigned char *)s2;
	if (n == 0)
		return (0);
	while (n--)
	{
		if (*c1 != *c2)
			return (*c1 - *c2);
		(n) ? c1++ : 0;
		(n) ? c2++ : 0;
	}
	return (0);
}
