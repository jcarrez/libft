/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_insert.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <jeremie.carrez@yahoo.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/27 14:58:27 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/27 14:58:29 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	ft_btree_insert(t_btree **lst, void *item, int (*f)(void *, void *))
{
	if (*lst == NULL)
		*lst = ft_btree_create(item);
	else
	{
		if (f(item, (*lst)->item) < 0)
			ft_btree_insert(&(*lst)->left, item, f);
		else if (f(item, (*lst)->item) >= 0)
			ft_btree_insert(&(*lst)->right, item ,f);
	}
}
