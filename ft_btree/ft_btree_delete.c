/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_delete.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <jeremie.carrez@yahoo.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/27 14:58:15 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/27 14:58:17 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	ft_btree_delete(t_btree *lst)
{
	t_btree	del_left;
	t_btree	del_right;

	del_left = *lst->left;
	del_right = *lst->right;
	if (lst != NULL)
	{
		ft_btree_delete(&del_left);
		ft_btree_delete(&del_right);
		free((void *)lst);
		lst = NULL;
	}
}
