/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_rev_infix.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <jeremie.carrez@yahoo.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/27 14:58:40 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/27 14:58:44 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	ft_btree_rev_infix(t_btree *lst, void (*f)(void *))
{
	if (lst != NULL)
	{
		ft_btree_rev_infix(lst->right, f);
		f(lst->item);
		ft_btree_rev_infix(lst->left, f);
	}
}
