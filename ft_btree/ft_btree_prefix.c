/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_prefix.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <jeremie.carrez@yahoo.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/27 14:58:34 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/27 14:58:35 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	ft_btree_prefix(t_btree *lst, void (*f)(void *))
{
	if (lst != NULL)
	{
		f(lst->item);
		ft_btree_prefix(lst->left, f);
		ft_btree_prefix(lst->right, f);
	}
}
