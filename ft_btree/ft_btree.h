/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <jeremie.carrez@yahoo.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/27 14:58:00 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/27 14:58:03 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_BTREE_H
#define FT_BTREE_H

#include "../libft.h"

typedef	struct	s_btree
{
	struct s_btree	*left;
	struct s_btree	*right;
	void			*item;
}				t_btree;

t_btree	*ft_btree_create(void *item);
void	ft_btree_delete(t_btree *lst);
void	ft_btree_infix(t_btree *lst, void (*f)(void *));
void	ft_btree_insert(t_btree **lst, void *item, int (*f)(void *, void *));
void	ft_btree_prefix(t_btree *lst, void (*f)(void *));
void	ft_btree_rev_infix(t_btree *lst, void (*f)(void *));
void	*ft_btree_search(t_btree *lst, void *item, int (*f)(void *, void *));
void	ft_btree_suffix(t_btree *lst, void (*f)(void *));

#endif
