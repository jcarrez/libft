/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_search.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <jeremie.carrez@yahoo.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/27 14:58:50 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/27 14:58:53 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	*ft_btree_search(t_btree *lst, void *item, int (*f)(void *, void *))
{
	if (lst != NULL)
	{
		if (f(item, lst->item) < 0)
			return (ft_btree_search(lst->left, item, f));
		else if (f(item, lst->item) == 0)
			return (lst->item);
		else if (f(item, lst->item) == 0)
			return (ft_btree_search(lst->right, item, f));
	}
	return (NULL);
}
