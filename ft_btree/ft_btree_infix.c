/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_infix.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <jeremie.carrez@yahoo.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/27 14:58:21 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/27 14:58:23 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	ft_btree_infix(t_btree *lst, void (*f)(void *))
{
	if (lst != NULL)
	{
		ft_btree_infix(lst->left, f);
		f(lst->item);
		ft_btree_infix(lst->right, f);
	}
}
