/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_suffix.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <jeremie.carrez@yahoo.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/27 14:58:58 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/27 14:59:06 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	ft_btree_suffix(t_btree *lst, void (*f)(void *))
{
	if (lst != NULL)
	{
		ft_btree_suffix(lst->left, f);
		ft_btree_suffix(lst->right, f);
		f(lst->item);
	}
}
