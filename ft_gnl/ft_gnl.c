/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gnl.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 23:39:38 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 10:51:49 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_gnl.h"

static int      ft_searchchar(char *str, char c)
{
	int nb;

	nb = 0;
	while (str[nb])
	{
		if (str[nb] == c)
			return (nb);
		nb++;
	}
	if (str[nb] == c)
		return (nb);
	return (-1);
}

static int      ft_gnl_collect(char **line, char *buf, t_list **list, int nd)
{
	char    *tmp;
	int     cut;

	if ((nd = ft_searchchar((*list)->content, '\n')) != -1)
	{
		tmp = *line;
		*line = ft_str_sub((*list)->content, 0, nd);
		free(tmp);
		tmp = (*list)->content;
		(*list)->content = ft_str_join((*list)->content, buf);
		free(tmp);
		tmp = (*list)->content;
		if ((cut = ft_searchchar((*list)->content, '\n')) > -1)
			(*list)->content = ft_str_sub((*list)->content, cut + 1,
					ft_len_str((*list)->content) - cut - 1);
		free(tmp);
		return (1);
	}
	else
	{
		tmp = (*list)->content;
		(*list)->content = ft_str_join((*list)->content, buf);
		free(tmp);
	}
	return (0);
}

static int      ft_gnl_c_line(int ret, char **line, char *buf, t_list **list)
{
	int     cut;
	char    *tmp;
	int     nd;

	nd = 0;
	buf[ret] = '\0';
	if (ft_gnl_collect(line, buf, list, nd))
		return (1);
	if (ret == 0)
	{
		tmp = *line;
		*line = ft_str_dup((*list)->content);
		free(tmp);
		tmp = (*list)->content;
		if ((cut = ft_searchchar((*list)->content, '\0')) > -1)
			(*list)->content = ft_str_sub((*list)->content,\
					cut + 1, ft_len_str((*list)->content) - cut);
		free(tmp);
		return (1);
	}
	return (0);
}

static t_list   *ft_gnl_fd(t_list **start, int fd)
{
	t_list  *tmp;
	t_list  *node;

	if (*start == NULL)
	{
		*start = ft_list_new("", 1);
		(*start)->content_size = fd;
		return (*start);
	}
	else
	{
		tmp = *start;
		while (tmp != NULL)
		{
			if ((int)tmp->content_size == fd)
				return (tmp);
			tmp = tmp->next;
		}
		node = ft_list_new("", 1);
		node->content_size = fd;
		ft_list_add(start, node);
		return (node);
	}
}

int             ft_gnl(const int fd, char **line)
{
	char            buf[BUFF_SIZE + 1];
	int             ret;
	static t_list   *start;
	t_list          *list;

	if (fd != -1 && line != NULL && BUFF_SIZE >= 1)
	{
		*line = ft_str_new(0);
		list = ft_gnl_fd(&start, fd);
		while ((ret = read(fd, buf, BUFF_SIZE)) > 0\
				|| 0 != ft_len_str(list->content))
		{
			buf[ret] = '\0';
			if (ft_gnl_c_line(ret, line, buf, &list))
				return (1);
		}
		if (ret == -1)
			return (-1);
		return (0);
	}
	return (-1);
}
