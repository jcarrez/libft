/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_new.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:46:31 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 11:09:29 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char    *ft_str_new(size_t size)
{
	void    *mem;

	if (!(mem = (void *)malloc(sizeof(void) * (size + 1))))
		return (NULL);
	ft_mem_set(mem, '\0', size + 1);
	return (mem);
}
