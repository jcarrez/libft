/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_merge.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:34:59 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 11:08:31 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char	*ft_str_merge(char *s1, char *s2)
{
	char	*out;

	if (s1 != NULL && s2 != NULL)
	{
		out = ft_str_join(s1, s2);
		ft_str_md(2, &s1, &s2);
		return (out);
	}
	return (NULL);
}
