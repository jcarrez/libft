/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_sub.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:51:33 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 11:13:55 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char    *ft_str_sub(char const *s, unsigned int start, size_t len)
{
    char            *tron;
    unsigned int    nb;

    nb = 0;
    if (s != NULL)
    {
        if (!(tron = (char *)malloc(sizeof(char) * (len + 1))))
            return (NULL);
        while (nb < len)
        {
            tron[nb] = (char)s[start + nb];
            nb++;
        }
        tron[nb] = '\0';
        return (tron);
    }
    return (NULL);
}
