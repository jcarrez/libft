/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_ndup.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:45:33 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 10:26:49 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char    *ft_str_ndup(const char *s1, size_t n)
{
	size_t  nb;
	char    *str;

	if (!(str = (char *)malloc(sizeof(*str) * (n + 1))))
		return (NULL);
	nb = 0;
	while (s1[nb] && nb < n)
	{
		str[nb] = s1[nb];
		nb++;
	}
	str[nb] = '\0';
	return (str);
}
