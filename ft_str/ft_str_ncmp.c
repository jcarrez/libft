/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_ncmp.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:40:47 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 10:25:35 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

int		ft_str_ncmp(const char *s1, const char *s2, size_t n)
{
	size_t	nb;

	nb = 0;
	if (n == 0)
		return (0);
	while (s1[nb] == s2[nb] && s1[nb] && s2[nb] && nb < n - 1)
		nb++;
	return ((unsigned char)s1[nb] - (unsigned char)s2[nb]);
}
