/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_iteri.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:11:56 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 11:02:50 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

void	ft_str_iteri(char *s, void (*f)(unsigned int, char *))
{
	int		a;

	a = -1;
	if (s != NULL && f != NULL)
		while (s[++a])
			f(a, &s[a]);
}
