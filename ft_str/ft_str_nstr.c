/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_nstr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:47:08 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 11:11:39 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char    *ft_str_nstr(const char *haystack, const char *needle, size_t len)
{
	size_t  nb;
	int     nbfind;
	char    *c1;

	c1 = (char*)haystack;
	nb = 0;
	nbfind = 0;
	if (ft_str_cmp(c1, "") == 0)
		return (NULL);
	if (ft_len_str(c1) == 0 || ft_len_str((char *)needle) == 0)
		return (c1);
	while (c1[nb] != '\0' && nb < len)
	{
		while (c1[nb + (unsigned long)nbfind] == needle[nbfind]
				&& needle[nbfind] != '\0' && nb + (unsigned long)nbfind < len)
			nbfind++;
		if (needle[nbfind] == '\0')
			return (c1 + nb);
		else
			nbfind = 0;
		nb++;
	}
	return (NULL);
}
