/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 10:18:22 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/01 22:27:43 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STR_H
# define FT_STR_H

# include "../libft.h"

char	*ft_str_cat(char *s1, const char *s2);
char	*ft_str_chr(const char *s, int c);
int		ft_str_chr_index(char *s, int c);
void	ft_str_clr(char *s);
int		ft_str_cmp(const char *s1, const char *s2);
char	*ft_str_cpy(char *d, const char *s);
void	ft_str_del(char **as);
char	*ft_str_ds(char **s);
char	*ft_str_dup(const char *s);
int		ft_str_equ(char const *s1, char const *s2);
void	ft_str_iter(char *s, void (*f)(char *));
void	ft_str_iteri(char *s, void (*f)(unsigned int, char *));
char	*ft_str_join(char const *s1, char const *s2);
void	ft_str_lc(char *s);
size_t	ft_str_lcat(char *dst, const char *src, size_t size);
char	*ft_str_map(char const *s, char (*f)(char));
char	*ft_str_mapi(char const *s, char (*f)(unsigned int, char));
void	ft_str_md(int s, ...);
char	*ft_str_merge(char *s1, char *s2);
char	*ft_str_ncat(char *s1, const char *s2, size_t n);
int		ft_str_ncmp(const char *s1, const char *s2, size_t n);
char	*ft_str_ncpy(char *dst, const char *src, size_t len);
char	*ft_str_ndup(const char *s1, size_t n);
int		ft_str_nequ(char const *s1, char const *s2, size_t n);
char	*ft_str_new(size_t size);
char	*ft_str_nstr(const char *haystack, const char *needle, size_t len);
char	*ft_str_rchr(const char *s, int c);
char	*ft_str_rev(char *str);
size_t	ft_str_rlen(char *s, int c);
char	*ft_str_sandr(char *s, char *to_find, char *replace);
char	**ft_str_split(char const *s, char c);
char	*ft_str_str(const char *haystack, const char *needle);
char	*ft_str_sub(char const *s, unsigned int start, size_t len);
char	*ft_str_trim(char const *s);
void	ft_str_uc(char *s);

#endif
