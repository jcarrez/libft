/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_dup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:02:30 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 11:53:01 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char	*ft_str_dup(const char *s)
{
	int		a;
	char	*s1;
	char	*s2;

	s1 = (char *)s;
	a = -1;
	if (!(s2 = (char *)malloc(sizeof(char) * (ft_len_str(s1) + 1))))
		return (NULL);
	while (++a < (int)ft_len_str(s1))
		s2[a] = s1[a];
	s2[a] = '\0';
	return (s2);
}
