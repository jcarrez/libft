/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_trim.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:52:01 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 12:16:37 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char    *ft_str_trim(char const *s)
{
	size_t  nb;
	char    *str;

	nb = 0;
	str = NULL;
	if (!s)
		return (NULL);
	if (!(str = ft_str_dup(s)))
		return (NULL);
	while (*str == ' ' || *str == '\n' || *str == '\t')
		str++;
	nb = ft_len_str(str) - 1;
	while (str[nb] == ' ' || str[nb] == '\n' || str[nb] == '\t')
		nb--;
	str[nb + 1] = '\0';
	return (ft_str_dup(str));
}
