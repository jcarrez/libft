/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_nequ.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:46:03 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 10:26:56 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

int     ft_str_nequ(char const *s1, char const *s2, size_t n)
{
	unsigned int    nb;

	if (s1 != NULL && s2 != NULL)
	{
		nb = 0;
		if (n == 0)
			return (1);
		while (s1[nb] == s2[nb] && s1[nb] != '\0' && s2[nb] != '\0'
				&& nb < n - 1)
			nb++;
		if (nb <= n - 1 && s1[nb] == s2[nb])
			return (1);
		return (0);
	}
	return (0);
}
