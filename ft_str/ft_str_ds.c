/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_ds.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/07 08:37:45 by jcarrez           #+#    #+#             */
/*   Updated: 2020/03/07 08:39:58 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char	*ft_str_ds(char **s)
{
	int		a;
	int		b;
	int		c;
	int		d;
	char	*r;

	b = 0;
	d = 0;
	a = ft_len_ss(s);
	if (!(r = (char *)malloc(sizeof(char) * a)))
		return (NULL);
	while (s[b])
	{
		c = 0;
		(b == 0) ?  0 : (r[d++] = ' ');
		while (s[b][c])
			r[d++] = s[b][c++];
		b++;
	}
	r[a - 1] = '\0';
	return (r);
}
