/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_sandr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/31 22:20:19 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/01 22:28:46 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char	*ft_str_sandr(char *s, char *to_find, char *replace)
{
	size_t	nb;
	char	*str;

	nb = 0;
	str = ft_str_dup(s);
	while (s[nb])
	{
		if (ft_str_cmp(str + nb, to_find) == 0)
			str[nb] = *replace;
		nb++;
	}
	return (str);
}
