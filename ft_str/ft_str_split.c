/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_split.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:50:17 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 11:17:27 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

static size_t	ft_count_words(char *s, char c)
{
	size_t	nb;
	size_t	w;
	size_t	is_w;

	nb = 0;
	w = 0;
	is_w = 0;
	while (s[nb])
	{
		if (is_w == 0 && s[nb] != c)
		{
			is_w = 1;
			w++;
		}
		else if (is_w == 1 && s[nb] == c)
			is_w = 0;
		nb++;
	}
	return (w);
}

static char		*ft_word(const char *str, char c, int *i)
{
	char    *s;
	int     nb;

	nb = 0;
	if (!(s = (char *)malloc(sizeof(char) * (ft_len_str((char *)str)))))
		return (NULL);
	while (str[*i] != c && str[*i])
	{
		s[nb] = str[*i];
		nb++;
		*i += 1;
	}
	s[nb] = '\0';
	while (str[*i] == c && str[*i])
		*i += 1;
	return (s);
}

char			**ft_str_split(char const *s, char c)
{
	int     nba;
	int     nbb;
	int     nbw;
	char    **str;

	nba = 0;
	nbb = 0;
	if (s != NULL)
	{
		nbw = ft_count_words((char *)s, c);
		if (!(str = (char **)malloc(sizeof(s) * (unsigned long)(nbw + 2))))
			return (NULL);
		while (s[nba] == c && s[nba])
			nba++;
		while (nbb < nbw && s[nba])
		{
			str[nbb] = ft_word(s, c, &nba);
			nbb++;
		}
		str[nbb] = NULL;
		return (str);
	}
	return (NULL);
}
