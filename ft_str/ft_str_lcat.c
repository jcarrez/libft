/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_lcat.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:28:03 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/01 21:29:35 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

size_t	ft_str_lcat(char *dst, const char *src, size_t size)
{
	size_t	a;
	size_t	b;
	size_t	ld;
	size_t	ls;

	ld = ft_len_str((char *)dst);
	ls = ft_len_str((char *)src);
	a = ld;
	b = 0;
	if (size <= ld)
		return (ls + size);
	while (src[b] != '\0' && a + b < size - 1)
	{
		dst[a + b] = src[b];
		b++;
	}
	dst[a + b] = '\0';
	return (ld + ls);
}
