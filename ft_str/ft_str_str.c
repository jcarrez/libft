/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_str.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:51:05 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 11:14:40 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char    *ft_str_str(const char *haystack, const char *needle)
{
	int     nb;
	int     nbfind;
	char    *c1;

	c1 = (char*)haystack;
	nb = 0;
	nbfind = 0;
	if (ft_len_str(c1) == 0 && ft_len_str((char *)needle) == 0)
		return (c1);
	while (c1[nb] != '\0')
	{
		while (c1[nb + nbfind] == needle[nbfind] && needle[nbfind] != '\0')
			nbfind++;
		if (needle[nbfind] == '\0')
			return (c1 + nb);
		else
			nbfind = 0;
		nb++;
	}
	return (NULL);
}
