/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_map.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:31:23 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 11:06:55 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char	*ft_str_map(char const *s, char (*f)(char))
{
	char	*result;
	int		nb;

	nb = -1;
	if (s != NULL && f != NULL)
	{
		if (!(result = (char *)malloc(sizeof(char)
		* (ft_len_str((char *)s) + 1))))
			return (0);
		while (s[++nb])
			result[nb] = f(s[nb]);
		result[nb] = '\0';
		return (result);
	}
	return (0);
}
