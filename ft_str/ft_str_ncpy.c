/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_ncpy.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:42:50 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 11:53:50 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char	*ft_str_ncpy(char *dst, const char *src, size_t len)
{
	int		nb;

	nb = -1;
	while (++nb < (int)len && src[nb] != '\0')
		dst[nb] = src[nb];
	while (nb < (int)len)
		dst[nb++] = '\0';
	return (dst);
}
