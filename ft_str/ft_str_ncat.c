/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_ncat.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:40:09 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 00:40:36 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char    *ft_str_ncat(char *s1, const char *s2, size_t n)
{
	unsigned int        nbd;
	unsigned int        nbs;

	nbd = 0;
	nbs = 0;
	while (s1[nbd] != '\0')
		nbd++;
	while (s2[nbs] != '\0' && nbs < n)
	{
		s1[nbd + nbs] = s2[nbs];
		nbs++;
	}
	s1[nbd + nbs] = '\0';
	return (s1);
}
