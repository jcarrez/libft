/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_rchr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:47:42 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 11:12:00 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char    *ft_str_rchr(const char *s, int c)
{
	size_t  nb;
	char    carac;
	char    *str;

	str = (char *)s;
	carac = (char)c;
	nb = ft_len_str(str);
	while (nb != 0 && str[nb] != carac)
		nb--;
	if (str[nb] == carac)
		return (str + nb);
	return (NULL);
}
