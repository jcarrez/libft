/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_join.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:13:33 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 11:03:27 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char	*ft_str_join(char const *s1, char const *s2)
{
	char	*join;
	size_t	a;
	size_t	b;

	a = -1;
	b = -1;
	if (s1 != NULL && s2 != NULL)
	{
		if (!(join = (char *)malloc(sizeof(char) * (ft_len_ms(1, s1, s2) + 1))))
			return (NULL);
		while (++a < ft_len_str((char *)s1))
			join[a] = s1[a];
		while (++b < ft_len_str((char *)s2))
			join[a + b] = s2[b];
		join[a + b] = '\0';
		return (join);
	}
	return (NULL);
}
