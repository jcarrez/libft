/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_mapi.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:34:19 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 11:07:39 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char    *ft_str_mapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned int    nb;
	char            *new;

	if (s != NULL & f != NULL)
	{
		nb = 0;
		if (!(new = (char *)malloc(sizeof(char) * (ft_len_str((char *)s) + 1))))
			return (0);
		while (s[nb])
		{
			new[nb] = f(nb, s[nb]);
			nb++;
		}
		new[nb] = '\0';
		return (new);
	}
	return (0);
}
