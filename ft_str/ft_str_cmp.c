/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_cmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 23:58:09 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/20 23:59:50 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

int		ft_str_cmp(const char *s1, const char *s2)
{
	size_t	a;

	a = 0;
	while (s1[a] == s2[a] && s1[a] && s2[a])
		a++;
	return ((unsigned char)s1[a] - (unsigned char)s2[a]);
}
