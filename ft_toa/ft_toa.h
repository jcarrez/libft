/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_toa.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 21:42:07 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/21 14:10:13 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef FT_TOA_H
# define FT_TOA_H

# include "../libft.h"

char	*ft_i_toa(int n);
char	*ft_st_toa(size_t n);
char	*ft_ui_toa(unsigned int n);

#endif
