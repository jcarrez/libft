/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/01 21:05:17 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/09 21:43:00 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_is.h"

int		ft_is(char c, char *s)
{
	size_t	a;

	a = 0;
	while (s[a])
	{
		if (c == s[a])
			return (Y);
		a++;
	}
	return (N);
}
