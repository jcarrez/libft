/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_uhex.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/02 13:49:30 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/02 17:40:57 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_is.h"

int		ft_is_up_h(int c)
{
	if ('A' <= c && c <= 'F')
		return (Y);
	return (N);
}
