/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 13:50:12 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/09 21:10:17 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_IS_H
# define FT_IS_H

# include "../libft.h"

# define Y 1
# define N 0

int		ft_is(char c, char *s);
int		ft_is_alnum(int c);
int		ft_is_alpha(int c);
int		ft_is_ascii(int c);
int		ft_is_d_digit(int c);
int		ft_is_digit(int c);
int		ft_is_h_digit(int c);
int		ft_is_hex(int c);
int		ft_is_lo(int c);
int		ft_is_lo_h(int c);
int		ft_is_print(int c);
int		ft_is_sign(int c);
int		ft_is_up(int c);
int		ft_is_up_h(int c);
int		ft_is_white(int c);

#endif
