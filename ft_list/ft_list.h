/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 17:56:00 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 10:55:43 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LIST_H
# define FT_LIST_H

# include "../libft.h"

typedef struct	s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}				t_list;

void			ft_list_add(t_list **alst, t_list *new);
void			ft_list_append(t_list *lst, t_list *new);
size_t			ft_list_count(t_list *lst);
void			ft_list_del(t_list **alst, void (*del)(void *, size_t));
void			ft_list_delone(t_list **alst, void (*del)(void *, size_t));
void			ft_list_iter(t_list *lst, void (*f)(t_list *elem));
t_list			*ft_list_map(t_list *lst, t_list *(*f)(t_list *elem));
t_list			*ft_list_new(void const *content, size_t content_size);
void			ft_list_rev(t_list **alst);

#endif
