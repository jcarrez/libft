/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_iter.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 17:45:25 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 10:58:18 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_iter(t_list *lst, void (*f)(t_list *elem))
{
	t_list *next;

	while (lst)
	{
		next = lst->next;
		f(lst);
		lst = next;
	}
}
