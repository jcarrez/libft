/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_at.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/02 13:57:17 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/02 13:59:15 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

t_list	*ft_list_at(t_list *lst, size_t nb)
{
	if ((lst == NULL) || (nb ==  1 && lst->next == NULL))
		return (NULL);
	if (nb == 0)
		return (lst);
	return (ft_list_at(lst->next, nb -1));
}
