/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_merge.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/02 14:03:31 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/02 14:05:44 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_merge(t_list *lst1, t_list *lst2)
{
	if ((*lst1) == NULL)
		*lst1 = lst2;
	if ((*lst1)->next == NULL)
		(*lst1)->next = lst2;
	ft_list_merge((*lst1)->next, lst2);
}
