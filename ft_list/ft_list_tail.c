/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_tail.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/02 14:01:24 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/02 14:02:28 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

t_list	*ft_list_tail(t_list *lst)
{
	if (lst == NULL)
		return (NULL);
	if (lst->next)
		return (ft_list_tail(lst->next));
	return (lst);
}
