/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_len_ms.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 00:21:01 by jcarrez           #+#    #+#             */
/*   Updated: 2020/02/21 10:52:42 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_len.h"

size_t	ft_len_ms(int s, ...)
{
	va_list	ap;
	int		t;
	size_t	ret;

	va_start(ap, s);
	t = -1;
	ret = 0;
	while (++t <= s)
		ret = ret + ft_len_str(va_arg(ap, char *));
	va_end(ap);
	return (ret);
}
