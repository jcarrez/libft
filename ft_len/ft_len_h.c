/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_len_h.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/05 21:00:06 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/05 21:00:59 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_len.h"

size_t	ft_len_h(int nb)
{
	size_t	l;

	l = 1;
	while (nb /= 16)
		l++;
	return (l);
}
