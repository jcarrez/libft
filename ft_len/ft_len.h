/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_len.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 14:46:47 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/21 14:12:00 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LEN_H
# define FT_LEN_H

# include "../libft.h"

size_t	ft_len_i(int n);
size_t	ft_len_ms(int s, ...);
size_t	ft_len_st(size_t n);
size_t	ft_len_ss(char **s);
size_t	ft_len_str(char *s);
size_t	ft_len_ui(unsigned int n);

#endif
