/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_len_o.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/05 21:01:35 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/05 21:02:15 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_len.h"

size_t	ft_len_o(int nb)
{
	size_t	l;

	l = 1;
	while (nb /= 8)
		l++;
	return (l);
}
