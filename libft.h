/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <jeremie.carrez@yahoo.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/27 14:57:12 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/27 14:57:15 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <string.h>
# include <stdlib.h>
# include <unistd.h>
# include <dirent.h>
# include <sys/ioctl.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <stdio.h>
# include <uuid/uuid.h>
# include <grp.h>
# include <time.h>
# include <pwd.h>
# include <sys/xattr.h>
# include <sys/acl.h>
# include <sys/errno.h>
# include <fcntl.h>
# include <stdarg.h>

# include "ft_ato/ft_ato.h"
# include "ft_btree/ft_btree.h"
# include "ft_gnl/ft_gnl.h"
# include "ft_i/ft_i.h"
# include "ft_is/ft_is.h"
# include "ft_len/ft_len.h"
# include "ft_list/ft_list.h"
# include "ft_math/ft_math.h"
# include "ft_mem/ft_mem.h"
# include "ft_put/ft_put.h"
# include "ft_str/ft_str.h"
# include "ft_toa/ft_toa.h"

#endif
