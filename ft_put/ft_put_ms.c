/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_ms.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/23 14:37:37 by jcarrez           #+#    #+#             */
/*   Updated: 2020/05/01 15:23:14 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_put.h"

size_t  ft_put_ms(int nb, ...)
{
	va_list	ap;
	int		n;
	size_t	ret;

	n = 0;
	ret = 0;
	va_start(ap, nb);
	while (n < nb)
	{
		ret += ft_put_str(va_arg(ap, char *));
		n++;
	}
	return (ret);
}
