/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_wchar.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/02 15:01:31 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/02 15:09:40 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_put.h"

size_t	ft_put_wchar(wchar_t c)
{
	size_t	a;

	a = 0;
	if (c <= 0x7F)
		a += ft_put_char(c);
	if (c <= 0x7FF)
	{
		a += ft_put_char((c >> 6) | 0xC0);
		a += ft_put_char((c & 0x3F) | 0x80);
	}
	if (c <= 0xFFFF)
	{
		a += ft_put_char((c >> 12) | 0xE0);
		a += ft_put_char(((c >> 6) & 0x3F) | 0x80);
		a += ft_put_char((c & 0x3F) | 0x80);
	}
	if (c <= 0x10FFFF)
	{
		a += ft_put_char((c >> 18) | 0xF0);
		a += ft_put_char(((c >> 12) & 0x3F) | 0x80);
		a += ft_put_char(((c >> 6) & 0x3F) | 0x80);
		a += ft_put_char((c & 0x3F) | 0x80);
	}
	return (a);
}
