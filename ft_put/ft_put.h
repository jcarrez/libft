/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 10:15:18 by jcarrez           #+#    #+#             */
/*   Updated: 2020/05/01 15:20:43 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PUT_H
# define FT_PUT_H

# include "../libft.h"

size_t	ft_put(char *s, ...);
size_t	ft_put_char(char c);
size_t	ft_put_char_fd(char c, int fd);
size_t	ft_put_endl(char *s, ...);
size_t	ft_put_fd(char *s, int fd);
size_t	ft_put_ms(int nb, ...);
size_t	ft_put_nchar(size_t len, char c);
size_t	ft_put_nchar_fd(size_t len, char c, int fd);
size_t	ft_put_ss(char **s, ...);
size_t	ft_put_st(size_t n);
size_t	ft_put_str(char *s, ...);

#endif
