/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_nchar_fd.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/01 21:48:38 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/01 22:22:38 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_put.h"

size_t	ft_put_nchar_fd(size_t len, char c, int fd)
{
	size_t	l;

	l = len;
	while (l--)
		write(fd, &c, 1);
	return (len);
}
