/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_endl.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/01 11:30:45 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/01 21:21:03 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_put.h"

size_t	ft_put_endl(char *s, ...)
{
	va_list	ap;
	int		fd;

	va_start(ap, s);
	fd = va_arg(ap, int);
	fd = (fd) ? 1 : fd;
	va_end(ap);
	return (ft_put_str(s, fd) + ft_put_str("\n", fd));
}
