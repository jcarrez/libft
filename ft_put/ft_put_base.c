/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_base.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/02 14:59:35 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/02 15:00:58 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_put.h"

size_t	ft_put_base(long double n, char *s)
{
	if (n >= ft_len_str(s))
		ft_put_base(n / ft_len_str(s), s);
	ft_put_char(s[n % ft_len_str(s)]);
}
