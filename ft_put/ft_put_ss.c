/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_ss.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/07 08:45:30 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/01 21:26:00 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_put.h"

size_t	ft_put_ss(char **s, ...)
{
	size_t	a;
	size_t	b;
	va_list	ap;
	int		fd;

	va_start(ap, s);
	fd = va_arg(ap, int);
	fd = (fd) ? 1 : fd;
	va_end(ap);
	a = 0;
	b = 0;
	while (s[b])
		a = a + ft_put_str(s[b++], fd);
	return (a);
}
