/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_i.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/06 13:31:14 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/06 13:35:08 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_I_H
# define FT_I_H

int		ft_i_abs(int a);
int		ft_i_cmp(int a, int b);
int		ft_i_factorial(int a);
int		ft_i_max(int a, int b);
int		ft_i_min(int a, int b);
int		ft_i_power(int a, int b);
int		ft_i_sqrt(int a);

#endif
