/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ato.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <jeremie.carrez@yahoo.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/27 14:57:24 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/27 14:57:27 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_ATO_H
# define FT_ATO_H

# include "../libft.h"

int					ft_ato_i(char *s);
unsigned int		ft_ato_ui(char *s);

#endif
