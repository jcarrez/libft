/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ato_i.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <jeremie.carrez@yahoo.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/27 14:57:31 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/27 14:57:33 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ato.h"

int		ft_ato_i(char *str)
{
	size_t	nb;
	int		res;
	int		sign;

	nb = 0;
	res = 0;
	sign = 1;
	if (str[nb] == '-' || str[nb] == '+')
	{
		if (str[nb] == '-')
			sign = -1;
		nb++;
	}
	while ('0' <= str[nb] && str[nb] <= '9')
	{
		res = res * 10 + str[nb] - '0';
		nb++;
	}
	return (res * sign);
}
