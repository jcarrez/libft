/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ato_ui.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcarrez <jeremie.carrez@yahoo.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/27 14:57:44 by jcarrez           #+#    #+#             */
/*   Updated: 2020/04/27 14:57:48 by jcarrez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ato.h"

unsigned int		ft_ato_ui(char *str)
{
	unsigned int	res;
	size_t			nb;

	res = 0;
	nb = 0;
	if (str[nb] == '+' || str[nb] == '-')
		nb++;
	while ('0' <= str[nb] && str[nb] <= '9')
	{
		res = res * 10 + str[nb] - '0';
		nb++;
	}
	return (res);
}
